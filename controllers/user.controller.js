const User = require ('../models/users.model');
const bcrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');
const {
    success, fail
} = require ('../helpers/res')

exports.loginUser = (req, res) => {
    User.findOne({email: req.body.email})
    .then(data => {
        
        if (bcrypt.compareSync(req.body.password, data.password)) {
            let token = jwt.sign({ email: req.body.email }, process.env.SECRET_KEY)
            res.status(200).json({
                status: true,
                data: {
                    email: data.email,
                    token: token
                }
            })
        } else {
            res.status(401).json({
                status: false,
                errors: "Wrong Password"
            })
        }
 })
    .catch(err => {
        res.status(400).json({
            status: false,
            errors: err
        })
    })
}

exports.addUser = async (req,res) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
            let user = new User({
                phone: req.body.phone,
                fullName: req.body.fullName,
                email: req.body.email,
                password: hash,
                gender: req.body.gender
            })
    user.save()
    .then(user => {
        res.status(200).json({
            status: true,
            data: user
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            errors: err
        });
    });

        })
    })
};

exports.getUser = (req, res) => {
    User.find()
    .then(data => {
        success(res, data, 200)
    })
    .catch(err => {
        fail(res, err, 400)
    })
};

exports.updateUser = (req, res) => {
    User.update({_id: req.params.user_id}, req.body)
    .then(data => {
        res.status(200).json({
            status: true,
            data
        })
    })
    .catch(err => {
        res.status(404).json({
            status: false,
            errors: err
        })
    })
}

exports.deleteUser = (req, res) => {
    User.deleteOne({_id: req.params.user_id}, req.body)
    .then(data => {
        res.status(200).json({
            status: true,
            data
        })
    })
    .catch(err => {
        res.status(404).json({
            status: false,
            errors: err
        })
    })
}