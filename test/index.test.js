const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("../server");
const User = require("../models/users.model");
const expected = chai.expect;
const bcrypt = require ('bcryptjs');

chai.use(chaiHttp);

describe("USER", () => {
  before(done => {
    User.deleteMany({}, err => {
      done();
    });
  });

  it("404", done => {
    chai
      .request(app)
      .get("/")
      .end((err, res) => {
        expected(res.status).eql(404);
        done();
      });
  });

  it("GET USER WITH 0 DATA", done => {
    chai
      .request(app)
      .get("/api/user")
      .end((err, res) => {
        expected(res.status).eql(200);
        expected(res.body.data.length).eql(0);
        done();
      });
  });

  it("ADD NEW USER", done => {
    chai
      .request(app)
      .post("/api/user")
      .send({
        phone: 08984089523,
        fullName: "Reza",
        email: "ezanirvana@gmail.com",
        password: "kaleng10",
        gender: "Male"
      })
      .end((err, res) => {
        expected(res.status).eql(200);
        done();
      });
  });

  it("ADD NEW USER", done => {
    chai
      .request(app)
      .post("/api/user")
      .send({
        phone: 081277153738,
        fullName: "Nirvana",
        email: "nirvana@mail.com",
        password: "kudakuda",
        gender: "Male"
      })
      .end((err, res) => {
        expected(res.status).eql(200);
        done();
      });
  });

  it("GET USER WITH 2 DATA", done => {
    chai
      .request(app)
      .get("/api/user")
      .end((err, res) => {
        expected(res.status).eql(200);
        expected(res.body.data.length).eql(2);
        done();
      });
  });

  it("LOGIN USER", done => {
    const user = {
      phone: 0812345678,
      fullName: 'mytest02',
      email: 'testing@mail01.com',
      password: 'kaleng',
      gender: 'Male'
    }
    let encrypted_password = bcrypt.hashSync(user.password, 10);
    let newUser = new User({
      ...user,
      password: encrypted_password
    });

    newUser.save().then((data) => {
    chai
    .request(app)
    .post("/api/user/login")
    .set('Content-Type', 'application/json')
    .send(JSON.stringify(user))
    .end((err, res) => {
      console.log(res.body)
      expected(res.status).eql(200);
      done();
    });
    });
  });
});
