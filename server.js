const express = require('express');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config()

const {
    MONGODB_URI
} = require('./mongo')
const router = require ('./router/user.router')

app.use(express.json());

mongoose.connect(MONGODB_URI, 
    {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
    .then(() =>
        console.log('Successfull Connected to database'))
    .catch(err =>
        console.log(err))

app.use('/api/user', router)    

module.exports = app;