const express = require("express");
const router = express.Router();

const {
  getUser,
  addUser,
  updateUser,
  deleteUser,
  loginUser
} = require("../controllers/user.controller");
const { logger } = require("../middlewares/logger");

router.get("/", logger, getUser);
router.post("/", logger, addUser);
router.put("/user_:id", logger, updateUser);
router.delete("/user_:id", logger, deleteUser);
router.post('/login', logger, loginUser);

module.exports = router;
