exports.success = (res, data, statusCode) => {
    return res.status(statusCode).json({
        status: true,
        data
    })
}

exports.fail = (res, err, statusCode) => {
    return res.status(statusCode).json({
        status: false,
        errors: err
    })
}